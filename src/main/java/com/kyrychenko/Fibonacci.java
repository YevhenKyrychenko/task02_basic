package com.kyrychenko;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Fibonacci class.
 */
public class Fibonacci {

    /**
     * constant percents.
     */
    private static final int ONE_HUNDRED_PERCENTS = 100;
    /**
     * Fibonacci numbers container.
     */
    private List<Integer> integers = new ArrayList<>();

    /**
     * generates Fibonacci numbers.
     * @param n - number of Fibonacci numbers
     * @return List
     */
    public final List<Integer> generate(final int n) {
        integers.add(0);
        if (n >= 2) {
            integers.add(1);
            IntStream.range(2, n)
                    .forEach(i ->
                            integers.add(integers.get(i - 1)
                                    + integers.get(i - 2)));
        }
        return integers;
    }

    /**
     * @return percentage of odd numbers
     */
    public final double getPercentageOfOddNumbers() {
        return (double) integers.stream()
                .mapToInt(Integer::intValue)
                .filter(i -> i % 2 == 1)
                .count() / integers.size() * ONE_HUNDRED_PERCENTS;
    }

    /**
     * @return percentage of even numbers
     */
    public final double getPercentageOfEvenNumbers() {
        return (double) integers.stream()
                .mapToInt(Integer::intValue)
                .filter(i -> i % 2 == 0)
                .count() / integers.size() * ONE_HUNDRED_PERCENTS;
    }

    /**
     * @return get max odd number
     */
    public final int getMaxOdd() {
        return integers.stream()
                .mapToInt(Integer::intValue)
                .filter(i -> i % 2 == 1)
                .max().getAsInt();
    }

    /**
     * @return get max even number
     */
    public final int getMaxEven() {
        return integers.stream()
                .mapToInt(Integer::intValue)
                .filter(i -> i % 2 == 0)
                .max().getAsInt();
    }
}
