package com.kyrychenko;

import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * main class - entry point.
 */
public final class Main {

    /**
     * constructor to avoid creating instances of this class.
     */
    private Main() {
    }

    /**
     *
     * @param args -str
     */
    public static void main(final String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Beginning of interval:");
        int start = in.nextInt();
        System.out.println("End of interval:");
        int ending = in.nextInt();

        printOddNumbers(start, ending);
        printEvenNumbers(start, ending);

        System.out.println("odds sum: " + getOddsSum(start, ending));
        System.out.println("evens sum: " + getEvenSum(start, ending));

        System.out.println("Enter size of Fibonacci set");
        int f = in.nextInt();
        Fibonacci fibonacci = new Fibonacci();
        List<Integer> integers = fibonacci.generate(f);
        integers.forEach(i -> System.out.print(i + " "));
        System.out.println();

        System.out.println("Max odd number");
        System.out.println(fibonacci.getMaxOdd());
        System.out.println("Max even number");
        System.out.println(fibonacci.getMaxEven());
        System.out.println("Percentage of odds "
                + fibonacci.getPercentageOfOddNumbers());
        System.out.println("Percentage of evens "
                + fibonacci.getPercentageOfEvenNumbers());
    }

    /**
     *
     * @param start - beginning of interval.
     * @param ending - ending of interval.
     */
    private static void printOddNumbers(final int start, final int ending) {
        System.out.println("Odd numbers");
        IntStream.rangeClosed(start, ending)
                .filter(i -> i % 2 == 1)
                .forEach(i -> System.out.print(i + " "));
        System.out.println();
    }

    /**
     *
     * @param start - beginning of interval.
     * @param ending - ending of interval.
     * @return odd numbers sum
     */
    private static int getOddsSum(final int start, final int ending) {
        return IntStream.rangeClosed(start, ending)
                .filter(i -> i % 2 == 1).sum();
    }

    /**
     *
     * @param start - beginning of interval.
     * @param ending - ending of interval.
     * @return even numbers sum
     */
    private static int getEvenSum(final int start, final int ending) {
        return IntStream.rangeClosed(start, ending)
                .filter(i -> i % 2 == 0).sum();
    }

    /**
     *
     * @param start - beginning of interval.
     * @param ending - ending of interval.
     */
    private static void printEvenNumbers(final int start, final int ending) {
        System.out.println("Even numbers");
        IntStream.rangeClosed(start, ending).filter(i -> i % 2 == 0)
                .boxed()
                .sorted(Collections.reverseOrder())
                .forEach(i -> System.out.print(i + " "));
        System.out.println();
    }
}
